package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractEntity {

    @Id
    @Nullable
    String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Nullable Status status;

    @Column(name = "dateOfCreate")
    @Nullable Date dateOfCreate;

    @Column(name = "dateOfBegin")
    @Nullable Date dateOfBegin;

    @Column(name = "dateOfEnd")
    @Nullable Date dateOfEnd;
}
