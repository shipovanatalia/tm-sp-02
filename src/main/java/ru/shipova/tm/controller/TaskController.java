package ru.shipova.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.util.DateFormatUtil;
import ru.shipova.tm.util.EntityConvertUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class TaskController {
    @Autowired
    @Nullable
    TaskService taskService;

    @Autowired
    @Nullable
    ProjectService projectService;

    @RequestMapping(value = "/task-list", method = RequestMethod.GET)
    public ModelAndView allTasks() {
        List<Task> taskList = taskService.getListTask();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = "/task-edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) {
        Task task = taskService.findOne(id);
        List<Project> projectList = projectService.getListProject();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projectList", projectList);
        modelAndView.addObject("dateBegin", DateFormatUtil.DateToString(task.getDateOfBegin()));
        modelAndView.addObject("dateEnd", DateFormatUtil.DateToString(task.getDateOfEnd()));
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = "/task-edit", method = RequestMethod.POST)
    public ModelAndView editTask(@ModelAttribute("task") TaskDTO taskDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/task-list");
        taskService.editDTO(taskDTO);
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = "/task-remove/{id}", method = RequestMethod.GET)
    public ModelAndView removeTask(@PathVariable("id") String id) {
        Task task = taskService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/task-list");
        taskService.remove(task);
        return modelAndView;
    }

    @RequestMapping(value = "/task-create", method = RequestMethod.GET)
    public ModelAndView createPage() {
        List<Project> projectList = projectService.getListProject();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-create");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @RequestMapping(value = "/task-create", method = RequestMethod.POST)
    public ModelAndView createTask(@ModelAttribute("task") Task task) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/task-list");
        taskService.create(task);
        return modelAndView;
    }

    @RequestMapping(value = "/task-view/{id}", method = RequestMethod.GET)
    public ModelAndView viewPage(@PathVariable("id") String id) {
        Task task = taskService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-view");
        modelAndView.addObject("task", task);
        modelAndView.addObject("dateBegin", DateFormatUtil.DateToString(task.getDateOfBegin()));
        modelAndView.addObject("dateEnd", DateFormatUtil.DateToString(task.getDateOfEnd()));
        return modelAndView;
    }

    @InitBinder
    private void dateBinder(WebDataBinder binder) {
        //The date format to parse or output your dates
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);
    }
}
