package ru.shipova.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.util.DateFormatUtil;
import ru.shipova.tm.util.EntityConvertUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    @Nullable ProjectService projectService;

    @RequestMapping(value = "/project-list", method = RequestMethod.GET)
    public ModelAndView allProjects() {
        List<Project> projectList = projectService.getListProject();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-list");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = "/project-edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("dateBegin", DateFormatUtil.DateToString(project.getDateOfBegin()));
        modelAndView.addObject("dateEnd", DateFormatUtil.DateToString(project.getDateOfEnd()));
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = "/project-edit", method = RequestMethod.POST)
    public ModelAndView editProject(@ModelAttribute("project") ProjectDTO projectDTO) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/project-list");
        projectService.editDTO(projectDTO);
        return modelAndView;
    }

    @RequestMapping(value = "/project-remove/{id}", method = RequestMethod.GET)
    public ModelAndView removeProject(@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/project-list");
        projectService.remove(project);
        return modelAndView;
    }

    @RequestMapping(value = "/project-create", method = RequestMethod.GET)
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-create");
        return modelAndView;
    }

    @RequestMapping(value = "/project-create", method = RequestMethod.POST)
    public ModelAndView createProject(@ModelAttribute("project") Project project) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/project-list");
        projectService.create(project);
        return modelAndView;
    }

    @RequestMapping(value = "/project-view/{id}", method = RequestMethod.GET)
    public ModelAndView viewPage(@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-view");
        modelAndView.addObject("project", project);
        modelAndView.addObject("dateBegin", DateFormatUtil.DateToString(project.getDateOfBegin()));
        modelAndView.addObject("dateEnd", DateFormatUtil.DateToString(project.getDateOfEnd()));
        return modelAndView;
    }

    @InitBinder
    private void dateBinder(WebDataBinder binder) {
        //The date format to parse or output your dates
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Create a new CustomDateEditor
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        //Register it as custom editor for the Date type
        binder.registerCustomEditor(Date.class, editor);
    }
}
