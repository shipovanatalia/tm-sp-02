package ru.shipova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;

public class EntityConvertUtil {
    @Nullable
    public static Project projectDTOToProject(@Nullable final ProjectDTO projectDTO){
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateOfBegin(projectDTO.getDateOfBegin());
        project.setDateOfEnd(projectDTO.getDateOfEnd());
        return project;
    }

    @Nullable
    public static Project dtoToProject(@Nullable ProjectDTO projectDTO) {
        if (projectDTO == null) return null;
        Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateOfBegin(projectDTO.getDateOfBegin());
        project.setDateOfEnd(projectDTO.getDateOfEnd());
        project.setStatus(projectDTO.getStatus());
        return project;
    }

    @Nullable
    public static Task DTOToTask(@Nullable final TaskDTO taskDTO){
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        @NotNull final Project project = new Project();
        project.setId(taskDTO.getProjectId());
        task.setProject(project);
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateOfBegin(taskDTO.getDateOfBegin());
        task.setDateOfEnd(taskDTO.getDateOfEnd());
        return task;
    }
}
