package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.entity.AbstractEntity;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.repository.ProjectDtoRepository;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.util.EntityConvertUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public class ProjectService{
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    public List<Project> getListProject() {
        List<Project> projectList = projectRepository.findAll();
        if (projectList == null) return new ArrayList<>();
        return projectList;
    }

    public void create(@Nullable final Project project) {
        project.setId(UUID.randomUUID().toString());
        project.setDateOfCreate(new Date());
        projectRepository.save(project);
    }

    public void remove(@Nullable final Project project) {
        projectRepository.delete(project);
    }

    @Nullable
    public Project findOne(@Nullable final String id){
        return projectRepository.findById(id).orElse(null);
    }

    public void edit(@Nullable final Project project){
        projectRepository.save(project);
    }

    public void editDTO(@Nullable final ProjectDTO projectDTO){
        projectDtoRepository.save(projectDTO);
    }
}
